%bcond_without perl_Class_Tiny_enables_optional_test
Name:                perl-Class-Tiny
Version:             1.008
Release:             2
Summary:             Minimalist class construction
License:             Apache-2.0
URL:                 https://metacpan.org/release/Class-Tiny
Source0:             https://cpan.metacpan.org/authors/id/D/DA/DAGOLDEN/Class-Tiny-%{version}.tar.gz
BuildArch:           noarch
BuildRequires:       make perl-generators perl-interpreter perl(:VERSION) >= 5.6
BuildRequires:       perl(ExtUtils::MakeMaker) >= 6.76 perl(strict) perl(warnings) perl(Carp)
BuildRequires:       perl(mro) perl(base) perl(Exporter) perl(File::Spec) perl(lib) perl(subs)
BuildRequires:       perl(Test::More) >= 0.96
%if %{with perl_Class_Tiny_enables_optional_test}
BuildRequires:       perl(Test::FailWarnings)
%endif
Requires:            perl(mro)
%global __requires_exclude %{?__requires_exclude:%__requires_exclude|}^perl\\(Devel::GlobalDestruction\\)
%description
This module offers a minimalist class construction kit in around 120 lines of code.

%prep
%autosetup -n Class-Tiny-%{version} -p1

%build
perl Makefile.PL INSTALLDIRS=vendor NO_PACKLIST=1
make %{?_smp_mflags}

%install
make pure_install DESTDIR=$RPM_BUILD_ROOT
%{_fixperms} $RPM_BUILD_ROOT/*

%check
make test

%files
%license LICENSE
%doc Changes CONTRIBUTING.mkdn README
%{perl_vendorlib}/*
%{_mandir}/man3/*

%changelog
* Tue Jan 14 2025 Funda Wang <fundawang@yeah.net> - 1.008-2
- cleanup spec

* Tue Jun 14 2022 SimpleUpdate Robot <tc@openeuler.org> - 1.008-1
- Upgrade to version 1.008

* Mon May 17 2021 Pengju Jiang <jiangpengju2@huawei.com> - 1.006-1
- package init
